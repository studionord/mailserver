var sendGridKey = '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';

function sendMail(emailToSend, emailData, callback){
	"use strict";
	var PIN = Math.floor(Math.random() * (999999 - 100000) + 100000);
	var sendgrid  = require('sendgrid')(sendGridKey);

	/*
	params

	var params = {
	  	smtpapi:  new sendgrid.smtpapi(),
	  	to:       [],
	  	toname:   [],
	  	from:     '',
	  	fromname: '',
	 	subject:  '',
	  	text:     '',
	  	html:     '',
	  	bcc:      [],
	  	cc:       [],
	 	replyto:  '',
	  	date:     '',
	  	files: [
	    	{
	      		filename:     '',           // required only if file.content is used.
	      		contentType:  '',           // optional
	      		cid:          '',           // optional, used to specify cid for inline content
	      		path:         '',           //
	      		url:          '',           // == One of these three options is required
	      		content:      ('' | Buffer) //
	    	}
	  	],
	  	file_data:  {},
	  	headers:    {}
	};

*/
	console.log("emailData:"+emailData);

	var obj = JSON.parse(emailData);

	var email     = new sendgrid.Email({
	  	to:       emailToSend,
	  	from:     obj.sender,
	  	fromname: obj.sendername,
	  	subject:  obj.subject,
	  	text:     obj.message,
	});
	
	sendgrid.send(email, function(err, message) {
		var json;
	  	if (err) { 
	  		console.error(err); 
	  		json = JSON.stringify({ 
				success:false,
				error:err
			});
	  	}else{
	  		console.log(message.message);
	  		json = JSON.stringify({ 
				success:true
			});
	  	}
		callback(json);
	});
}

function saveToNewsletter(emailData, callback){
	"use strict";
	var obj = JSON.parse(emailData);
	console.log(obj.email);

    var json = JSON.stringify({
            success:true,
        });

    var sendgrid  = require('sendgrid')(sendGridKey);

	var fs = require('fs');

	var fileName = 'public/newsletter.html';

	fs.readFile(fileName, 'utf8', function(err, contents) {

		var htmlContents = contents;

		var email     = new sendgrid.Email({
			to:       obj.email,
			from:     obj.sender,
			fromname: obj.sendername,
			subject:  obj.subject,
			html:htmlContents
		});

		sendgrid.send(email, function(err, message) {
			var json;
			if (err) {
				console.error(err);
			}else{
				console.log(message.message);
			}
		});
	});

	callback(json);
}

function sendMailFromTemplate(emailToSend, emailData, templateName, callback){
	"use strict";
	var PIN = Math.floor(Math.random() * (999999 - 100000) + 100000);
	var sendgrid  = require('sendgrid')(sendGridKey);


	/*
	params

	var params = {
	  	smtpapi:  new sendgrid.smtpapi(),
	  	to:       [],
	  	toname:   [],
	  	from:     '',
	  	fromname: '',
	 	subject:  '',
	  	text:     '',
	  	html:     '',
	  	bcc:      [],
	  	cc:       [],
	 	replyto:  '',
	  	date:     '',
	  	files: [
	    	{
	      		filename:     '',           // required only if file.content is used.
	      		contentType:  '',           // optional
	      		cid:          '',           // optional, used to specify cid for inline content
	      		path:         '',           //
	      		url:          '',           // == One of these three options is required
	      		content:      ('' | Buffer) //
	    	}
	  	],
	  	file_data:  {},
	  	headers:    {}
	};

*/
	console.log("emailData:"+emailData);
	
	var obj = JSON.parse(emailData);
	var fs = require('fs');

    var fileName = "";

    if (templateName == "welcome"){
        fileName = 'public/welcome.html';
    }

	if (templateName == "complete"){
		fileName = 'public/complete.html';
	}

	if (templateName == "activation"){
		fileName = 'public/activation.html';
	}

	if (templateName == "forgotpassword"){
		fileName = 'public/forgot.html';
	}

	fs.readFile(fileName, 'utf8', function(err, contents) {

		var htmlContents = contents;

        if (templateName=="welcome"){
        }else if(templateName=="activation"){
			htmlContents = htmlContents.replace("<%link%>", obj.activationURL);
        }else if(templateName=="forgotpassword"){
			htmlContents = htmlContents.replace("<%link%>", obj.resetURL);
        }else if(templateName=="complete"){
			htmlContents = htmlContents.replace("<%message%>", obj.emailTitle);
		}

		var email     = new sendgrid.Email({
			to:       emailToSend,
			from:     obj.sender,
			fromname: obj.sendername,
			subject:  obj.subject,
			html:htmlContents
		});

		sendgrid.send(email, function(err, message) {
			var json;
			if (err) {
				console.error(err);
				json = JSON.stringify({
					success:false,
					error:err
				});
			}else{
				console.log(message.message);
				json = JSON.stringify({
					success:true
				});
			}
			callback(json);
		});
	});
}

module.exports.sendMail = sendMail;
module.exports.sendMailFromTemplate = sendMailFromTemplate;
module.exports.saveToNewsletter = saveToNewsletter;

var express     = require('express');
var bodyParser  = require('body-parser');
var fs          = require('fs');
var morgan      = require('morgan');
var mail        = require('./mw/mail');
var mongoose    = require('mongoose');
var User        = require('./mw/user');
var app         = express();

var accessLogStream = fs.createWriteStream(__dirname + '/logs/access.log', {flags: 'a'}) // setup the logger

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.use(morgan('combined', {stream: accessLogStream}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set('port', (process.env.PORT || 3006));

mongoose.connect('mongodb://localhost/newsletter');

app.use(function (request, response) {
  var postData = request.body;

  console.log(postData);

  if(postData.action == "sendmail"){
    var emailParams = JSON.stringify({ 
      sender:postData.sender,
      sendername:postData.sendername,
      subject:postData.subject,
      message:postData.message,
    });
    mail.sendMail(postData.email, emailParams, function(state){
      console.log(state);
      response.send(state);
    });

  }else if(postData.action == "pushcomplete") {

      var emailParams = JSON.stringify({
          sender: postData.sender,
          sendername: postData.sendername,
          subject: postData.subject,
          emailTitle:postData.emailTitle
      });

      mail.sendMailFromTemplate(postData.email, emailParams, "complete", function (state) {
          console.log(state);
          response.send(state);
      });

  }else if(postData.action == "sendwelcome") {
      var emailParams = JSON.stringify({
          sender: postData.sender,
          sendername: postData.sendername,
          subject: postData.subject
      });
      mail.sendMailFromTemplate(postData.email, emailParams, "welcome", function (state) {
          console.log(state);
          response.send(state);
      });
  }else if(postData.action=="sendactivation"){
      var emailParams = JSON.stringify({
          sender: postData.sender,
          sendername: postData.sendername,
          subject: postData.subject,
          activationURL:postData.activationURL
      });
      mail.sendMailFromTemplate(postData.email, emailParams, "activation", function (state) {
          console.log(state);
          response.send(state);
      });
  }else if(postData.action=="forgotpassword") {
      var emailParams = JSON.stringify({
          sender: postData.sender,
          sendername: postData.sendername,
          subject: postData.subject,
          resetURL: postData.resetURL
      });
      mail.sendMailFromTemplate(postData.email, emailParams, "forgotpassword", function (state) {
          console.log(state);
          response.send(state);
      });
  }else if(postData.action == "newsletter"){

      var json = JSON.stringify({
          success:false
      });

      User.findOne({ email: postData.email }, function(err, user) {
          if (err) {
              response.send(json);
          }else{
              if (user==null){
                  var user = new User({ email:postData.email,created_at:Date.now() });
                  user.save(function (err) {
                      if (err) {
                          response.send(json);
                      } else {
                          var emailParams = JSON.stringify({
                              "email":postData.email,
                              sender: "info@connectbox.co",
                              sendername: "ConnectBox",
                              subject: "Welcome to Our Newsletter"
                          });
                          mail.saveToNewsletter(emailParams, function (state2) {
                              response.send(state2);
                          });
                      }
                  });
              }else{
                  console.log("user found");
                  json = JSON.stringify({
                      success:true
                  });
                  response.send(json);
              }
          }
      });

  }else{
    response.send("...");
  }
});

app.get('/', function(request, response){
  response.send("mail");
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});



